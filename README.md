# sample-inventory-2

Inventory repository for product.

Includes:
- `group_vars` - product group_vars. Folder is not in zone folder for single responsibility pattern.
- `production`, `staging`, `test` - ansible host lists for environments.
- `extra_vars.yml` - parameters for installer bootstrap.
- `workflow.json` - parameters for jenkins workflow

Before instalation starts, jenkins will copy `group_vars` folder to current environment folder. This makes ansible inventory valid.